import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import Layout from "./hoc/Layout/Layout";
import RegisterPage from "./containers/RegisterPage/registerPage";
import HomePage from "./containers/HomePage/homePage";
import BookPage from "./containers/BookPage/BookPage";
import UberWorkPage from "./containers/uberWorkPage/UberWorkPage";
import ToGoPage from "./containers/toGoPage/toGoPage";
import UberCarePage from "./containers/uberCarePage/uberCarePage";
import ContactPage from "./containers/ContactPage/contactPage";
import FromToPage from "./containers/FromToPage/FromToPage";
class App extends Component {
  state = {
    fullname: ""
  };

  componentWillMount() {
    if (localStorage && localStorage.getItem("fullname")) {
      const fullname = localStorage.getItem("fullname");
      this.setState({
        fullname
      });
    }
  }
  renderRedirect = () => {
    if (this.state.fullname) {
      return <Redirect to="/book" />;
    }
  };
  render() {
    return (
      <div>
        <Layout>
          <Router>
            <Switch>
              <Route path="/book" component={BookPage} />
              <Route path="/register" component={RegisterPage} />
              <Route path="/uberwork" component={UberWorkPage} />
              <Route path="/ubercare" component={UberCarePage} />
              <Route path="/togo" component={ToGoPage} />
              <Route path="/fromto" component={FromToPage} />
              <Route path="/contact" component={ContactPage} />
              {this.renderRedirect()}
              <Route path="/" exact component={HomePage} />
            </Switch>
          </Router>
        </Layout>
      </div>
    );
  }
}

export default App;
