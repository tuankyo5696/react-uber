import React, { Component } from "react";
import MapFromTo from "./../../components/Map/FromTo/MapFromTo";
class FromTo extends Component {
  render() {
    return (
      <div>
        <MapFromTo
          google={this.props.google}
          center={{ lat: 18.5204, lng: 73.8567 }}
          height="80vh"
          zoom={15}
        />
      </div>
    );
  }
}

export default FromTo;
