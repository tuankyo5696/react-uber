import React from 'react'
import BookPage from './BookPage';
import TestRenderer from "react-test-renderer";
import { shallow, render, mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

global.shallow = shallow;
global.render = render;
global.mount = mount;

describe("Bookpage Test ", () => {
    it("Should render snapshot for date picker", () => {
      const wrapper = TestRenderer.create(<BookPage/>).toJSON()
      expect(wrapper).toMatchSnapshot()
    });
  });