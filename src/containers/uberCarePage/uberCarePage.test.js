import React from 'react'
import UberCarePage from './uberCarePage';
import TestRenderer from "react-test-renderer";
import { shallow, render, mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

global.shallow = shallow;
global.render = render;
global.mount = mount;

describe("UberCarePage Test ", () => {
    it("Should render snapshot for date picker", () => {
      const wrapper = TestRenderer.create(<UberCarePage/>).toJSON()
      expect(wrapper).toMatchSnapshot()
    });
  });