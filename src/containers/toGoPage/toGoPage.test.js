import React from 'react'
import ToGoPage from './toGoPage';
import TestRenderer from "react-test-renderer";
import { shallow, render, mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

global.shallow = shallow;
global.render = render;
global.mount = mount;

describe("ToGoPage Test ", () => {
    it("Should render snapshot for date picker", () => {
      const wrapper = TestRenderer.create(<ToGoPage/>).toJSON()
      expect(wrapper).toMatchSnapshot()
    });
  });