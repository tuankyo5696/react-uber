import React, { Component } from "react";
import MapToGo from "./../../components/Map/Togo/MapToGo";
class ToGo extends Component {
  render() {
    return (
      <div>
        <MapToGo
          google={this.props.google}
          center={{ lat: 18.5204, lng: 73.8567 }}
          height="80vh"
          zoom={15}
        />
      </div>
    );
  }
}

export default ToGo;
