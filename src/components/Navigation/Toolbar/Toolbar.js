import React, { Component } from 'react';

import './Toolbar.scss';
import Logo from './../../Logo/Logo';
import ToggleButton from './../../UI/Button/ToggleButton/ToggleButton';
import { connect } from 'react-redux'
import Male from './../../../assets/img/male.png'
import Female from './../../../assets/img/female.png'
import './Toolbar.scss';
class Toolbar extends Component {

    render() {
        let gender = ''
        if (this.props.auth.gender) {
            gender = this.props.auth.gender === 'male' ? <img className="imgGender" src={Male} alt='Male' /> : <img className="imgGender" src={Female} alt='Female' />
        }
        return (
            <header className="Toolbar" >
                <div className="LogoIcon">
                    <Logo />
                </div>
                <div className="headerRight">
                    <div className="genderHeader">
                        {gender}
                    </div>
                    <div className="fullNameHeader">
                        <p>{this.props.auth.fullname}    </p>
                    </div>
                    <div className="ToggleButton">
                        <ToggleButton />
                    </div>
                </div>



            </header>
        )
    }
}

const mapStateToProps = state => {
    return {
        auth: state.user.auth
    }
}
export default connect(mapStateToProps, null)(Toolbar);