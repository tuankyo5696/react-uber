import React from "react";
import "./FromTo.scss";
import { withRouter } from "react-router-dom";

class FromToBook extends React.Component {
  state = {
    addressFrom: "",
    addressTo: ""
  };
  componentWillMount() {
    if (
      localStorage &&
      localStorage.getItem("address") &&
      localStorage.getItem("addressTo")
    ) {
      const addressFrom = localStorage.getItem("address");
      const addressTo = localStorage.getItem("addressTo");
      this.setState({ 
        addressFrom,
        addressTo
      });
    }
  }
  goBackToGo = () => {
    this.props.history.goBack();
  };
  goHome = () => {
    alert("You book sucessfully");
    this.props.history.push("/");
  };
  render() {
    return (
      <div>
        <div className="Pickup">
          <div className="placeContainer">
            <div className="bodyFromTo">
              <div className="txtFT">
                <div className="FTDiv flex1">
                  <div className="imgFromTo">
                    <i className="fas fa-map-marker-alt pl-2" />
                  </div>
                  <div className="fromToDetails">
                    <p>From</p>
                    <p className="txtDetails grayColor">
                      {this.state.addressFrom}
                    </p>
                  </div>
                </div>
                <div className="FTDiv flex1">
                  <div className="imgFromTo">
                    <i className="fab fa-telegram-plane pl-2" />
                  </div>
                  <div className="fromToDetails">
                    <p>To</p>
                    <p className="txtDetails grayColor">
                      {this.state.addressTo}
                    </p>
                  </div>
                </div>
                <div className="confirmDiv flex1">
                  <div className="backFromTo">
                    <div className="selectFT">
                      <div className="custom-selectFTelm">
                        <div className="elmFT">
                          <p className="txtPrice">Prize: $9,76 </p>
                          <p className="txtDetails">Kilomets: 3.85km</p>
                          <p className="txtDetails">Time: 35minutes</p>
                        </div>
                      </div>
                      <div className="selectFTelm custom-selectFT">
                        <select className="selectFTOptions">
                          <option value="0">Fare breakdown</option>
                          <option value="1">Tunnel</option>
                          <option value="2">high speed train</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="nextFromTo flex1">
                  <div className="backFTbtn">
                    <button className="btnBackFT" onClick={this.goBackToGo}>
                      <div>
                        <i className="fas fa-arrow-left flex1" />
                      </div>
                      <div>
                        <p className="txtPrice flex1">Back</p>
                      </div>
                    </button>
                  </div>
                  <div className="row">
                    <button
                      className="register"
                      type="submit"
                      onClick={this.goHome}
                    >
                      <p className="white">Register with Phone</p>
                      <i className="fas fa-arrow-right white" />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default withRouter(FromToBook);
