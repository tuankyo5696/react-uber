import React from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng
} from "react-places-autocomplete";
import "./Togo.scss";
import imgFood from "./../../../assets/img/food.jpg";
import imgFoodLunch from "./../../../assets/img/lunch.jpg";
import imgF1 from "./../../../assets/img/friend1.png";
import imgF2 from "./../../../assets/img/friend2.png";
import imgF3 from "./../../../assets/img/friend3.png";
import { withRouter } from "react-router-dom";

class ToGoSearchInput extends React.Component {
  state = {
    address: "",
    suggestion: [
      {
        mainText: "Exeter",
        secondaryText: "Vương Quốc Anh"
      },
      {
        mainText: "Eindhovenr",
        secondaryText: "Hà Lan"
      },
      {
        mainText: "ETown",
        secondaryText: "Cộng Hòa, phường 13, Tân Bình, Hồ Chí Minh, Việt Nam"
      },
      {
        mainText: "Nguyễn Văn Luông",
        secondaryText: "Quận 6, Hồ Chí Minh, Việt Nam"
      }
    ]
  };
  componentWillMount() {
    if (localStorage && localStorage.getItem("addressTo")) {
      const addressTo = localStorage.getItem("addressTo");
      this.setState({
        address: addressTo
      });
    }
  }
  handleChange = address => {
    this.setState({ address });
  };

  handleSelect = address => {
    localStorage.setItem("addressTo", address);
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => this.props.changePosition(latLng))
      .catch(error => console.error("Error", error));
    this.setState({ address: address });
    console.log(this.state.address);
  };
  suggestionChoose = address => {
    this.setState({
      address
    });
  };
  goBackToBook = () => {
    this.props.history.goBack();
  };
  addressResult = () => {
    const address = this.state.address;
    if (address) {
      localStorage.setItem("addressTo", address);
      this.props.history.push("/fromto");
    }
  };
  render() {
    return (
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
      >
        {({
          getInputProps,
          suggestions,
          getSuggestionItemProps,
          loading,
          value
        }) => {
          const renderSuggestion = (
            <div className="bodyTogo">
              <div className="txt">
                <div className="office">
                  <div className="groupElm">
                    <i className="fas fa-briefcase" />
                  </div>
                  <div className="groupTxt">
                    <p className="bold">Ofﬁce </p>
                    <p className="details">
                      BDlive24,Road 8 Niketon, gulshan, 1000{" "}
                    </p>
                  </div>
                </div>
                <div className="office">
                  <div className="groupElm">
                    <i className="fas fa-home" />
                  </div>
                  <div className="groupTxt">
                    <p className="bold">Home </p>
                    <p className="details">
                      BDlive24,Road 8 Niketon, gulshan, 1000{" "}
                    </p>
                  </div>
                </div>
              </div>

              <div className="groupImg">
                <div className="imgP">
                  <p>Nearby Restaurants</p>
                </div>
                <div className="twoImg">
                  <div className="elmImg">
                    <img className="imgF" src={imgFood} alt="logfood" />
                  </div>
                  <div className="elmImg">
                    <img className="imgF" src={imgFoodLunch} alt="logfood" />
                  </div>
                </div>
              </div>
              <div className="friend">
                <div>
                  <p>Friend nearby you</p>
                </div>
                <div className="groupFriend">
                  <div className="friendGroup">
                    <div className="imgElm">
                      <img src={imgF1} alt="friend1" />
                    </div>
                    <div className="txtFriend">
                      <p className="name">Smirk</p>
                      <p className="Dha">Dhanmondi 32</p>
                    </div>
                  </div>
                  <div className="friendGroup">
                    <div className="imgElm">
                      <img src={imgF2} alt="friend2" />
                    </div>
                    <div className="txtFriend">
                      <p className="name">Rayhan</p>
                      <p className="Dha">Dhanmondi 32</p>
                    </div>
                  </div>
                  <div className="friendGroup">
                    <div className="imgElm">
                      <img src={imgF3} alt="friend3" />
                    </div>
                    <div className="txtFriend">
                      <p className="name">Farhana </p>
                      <p className="Dha">Dhanmondi 32</p>
                    </div>
                    
                  </div>
                </div>
              </div>
              <button className="btnBackFT" onClick={this.goBackToBook}>
                      <div>
                        <i className="fas fa-arrow-left flex1" />
                      </div>
                      <div>
                        <p className="txtPrice flex1">Back</p>
                      </div>
                    </button>
            </div>
          );
          return (
            <div>
              <div className="Pickup">
                <div className="placeContainer">
                  <label htmlFor="square">
                    <i className="fas fa-square" />
                  </label>
                  <input
                    {...getInputProps({
                      placeholder: "Search Places ...",
                      className: "location-search-input"
                    })}
                  />
                  <button className="Button" onClick={this.addressResult}>
                    <i className="fas fa-search" />
                  </button>
                  <div className="autocomplete-dropdown-container">
                    {loading && <div>Loading...</div>}
                    {this.state.address.length > 0
                      ? suggestions.map(suggestion => {
                          return (
                            <div
                              className="suggestion"
                              {...getSuggestionItemProps(suggestion)}
                            >
                              <i className="fas fa-map-marker-alt" />
                              <div className="place-group">
                                <h6>
                                  {suggestion.formattedSuggestion.mainText}
                                </h6>
                                <p>
                                  {suggestion.formattedSuggestion.secondaryText}
                                </p>
                              </div>
                            </div>
                          );
                        })
                      : renderSuggestion}
                  </div>
                </div>
              </div>
            </div>
          );
        }}
      </PlacesAutocomplete>
    );
  }
}
export default withRouter(ToGoSearchInput);
