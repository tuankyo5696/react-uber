import React from "react";
import "./uberCare.scss";

const uberCare = props => (
  <div className="pageCare">
    <div className="coverCare">
      <form className="howUberCare">
        <div className="how">
          <p className="atUber">
            At Uber we care about you & the trust you give us
          </p>
          <p className="uberPut">
            Uber puts transportation at your fingertips. Understanding how and
            why we use your information should be just as easy. Our Privacy
            Policy and Privacy FAQs provide the latest information about the
            data we collect and how we use it. We also want you to know a few
            more things about our approach to privacy:
          </p>
        </div>
        <br />

        <div className="thirdStep">
          <div className="coverAllTitle">
            <div className="step1">
              <p className="contactP titleBold">Your data works for you</p>
              <p className="elm">
                We use your data to provide convenient transportation and
                delivery options, to help drivers maximize their earnings, and
                to protect the safety and security of our users. And we don’t
                rent or sell your data -- to anyone.
              </p>
            </div>
            <div className="step2">
              <p className="contactP titleBold">Safety first</p>
              <p className="elm">
                Your data enables us to determine the safest pick-ups and
                drop-offs, deter unsafe driving habits, help you avoid
                accident-prone roads, and to develop new features to get where
                you’re going, safe and sound.
              </p>
            </div>
            <div className="step3">
              <p className="contactP titleBold">You are in control</p>
              <p className="elm">
                Through the settings in the Uber app or on your device, you can
                choose when to share your location, sync your contacts and
                calendar, personalize your app, and even delete your account.
              </p>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
);
export default uberCare;
