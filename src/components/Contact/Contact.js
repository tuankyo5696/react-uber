import React from "react";
import "./Contact.scss";
import { Formik, Field } from "formik";
import * as Yup from "yup";
import rightArrow from "./../../assets/img/rightArrow.png";
const contact = props => (
  <div className="pageContact">
    <div className="coverContact">
      <Formik
        initialValues={{
          name: "",
          message: ""
        }}
        validationSchema={Yup.object().shape({
          name: Yup.string()
            .min(8, "Too Short!")
            .max(50, "Too long!")
            .required("name is required"),
          message: Yup.string()
            .min(8, "Too Short!")
            .max(50, "Too long!")
            .required("Message is required")
        })}
      >
        {({ errors, touched }) => (
          <form className="FormContact">
            <div className="enter enterContact">
              <i className="fas fa-envelope" />
              <p className="contactP">Contact us</p>
            </div>
            <div className="detailsContact">
              <div className="leftGroup flex2">
                <div className="group-inline">
                  <div className="coverFormInput form-control-contact">
                    <div className="selectGender flex2">
                      <p className="resize-contact">Your Name:</p>
                    </div>
                    <div>
                      <Field
                        type="text"
                        name="name"
                        className="inputCont"
                        placeholder="Enter the issue"
                      />
                    </div>
                  </div>
                </div>
                {errors.name && touched.name ? (
                  <div className="inputValid">{errors.name}</div>
                ) : null}
                <div className="group-inline coverFormInput groupSelect">
                  <div className="select">
                    <p className="selectGender resize-contact select">
                      Select:
                    </p>
                  </div>

                  <div className="custom-select">
                    <select>
                      <option value="0">Rider</option>
                      <option value="1">Audi</option>
                      <option value="2">BMW</option>
                      <option value="9">Mini</option>
                      <option value="10">Nissan</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="rightGroup group-inline coverFormInput flex2">
                <p className="selectGender resize-contact">Message:</p>

                <div>
                  <Field
                    type="textarea"
                    name="message"
                    className="inputCont"
                    placeholder="Enter the issue"
                  />
                </div>
                {errors.message && touched.message ? (
                  <div className="inputValid">{errors.message}</div>
                ) : null}
              </div>
            </div>

            <div className="flex2">
              <p />
            </div>

            <div className="rightArrow">
              <button className="rightArrBtn" type="submit">
                <img className="rightArrowImg" src={rightArrow} alt="" />
              </button>
            </div>
          </form>
        )}
      </Formik>
    </div>
  </div>
);

export default contact;
