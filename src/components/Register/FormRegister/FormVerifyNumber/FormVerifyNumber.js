import React from "react";
import "./FormVerifyNumber.scss";
import { FieldArray } from "formik";
import btnArr from "./../../../../assets/img/rightArrow.png";

const Step2 = ({
  handleChange,
  values,
  prevStep,
  nextStep,
  errors,
  handleBlur
}) => {
  const handleOnChange = event => {
    if (event.target.value.length > 0) {
      event.target.value = event.target.value[0];
    }
    handleChange(event);
  };
  return (
    <div className="coverNum">
      <div className="Form">
        <div className="Enter">
          <div className="digit">
            <p>
              Enter the 4-digit code sent to you at {values.phone}.
              <span className="correct">did you enter the correct number?</span>
              <span onClick={prevStep}>Do you get back ?</span>
            </p>
          </div>
        </div>

        <FieldArray
          name="securityNumber"
          render={arrayHelpers => (
            <div className="numbers">
              {values.securityNumber.map((number, index) => (
                <div key={index}>
                  <input
                    name={`securityNumber.${index}`}
                    key={index}
                    value={values.securityNumber[index]}
                    onChange={handleOnChange}
                    onBlur={handleBlur}
                    className="num"
                  />
                </div>
              ))}
            </div>
          )}
        />
        <div className="securityNum">
          {errors && (
            <div id="feedback">
              {errors.securityNumber ? "Security Number is required" : ""}
            </div>
          )}
        </div>

        <div className="didnt">
          <div className="code">
            <a href="#/" className="receiveCode">
              I didn’t receive code
            </a>
          </div>
          <div className="nextBtn">
            <button
              onClick={nextStep}
              className="btnArr"
              disabled={errors.securityNumber ? true : false}
            >
              <img src={btnArr} alt="ImgArr" className="ImgArr" />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Step2;
