import React from "react";
import { withFormik } from "formik";
import Step1 from "./FormPhoneNumber/FormPhoneNumber";
import Step2 from "./FormVerifyNumber/FormVerifyNumber";
import Step3 from "./FormProfile/FormProfile";
import { withRouter } from "react-router";
import { compose, withState, withHandlers } from "recompose";
import { connect } from "react-redux";
import * as actions from "./../../../store/actions/index";

const numberRegex = new RegExp("^[0-9]+$");
const enhance = compose(
  withState("step", "setStep", 1),
  withHandlers({
    nextStep: ({ setStep, step }) => () => setStep(step + 1),
    prevStep: ({ setStep, step }) => () => setStep(step - 1)
  }),
  withFormik({
    mapPropsToValues: ({
      form: { phone, fullname, singleCheckbox, gender, securityNumber }
    }) => ({
      phone,
      fullname,
      singleCheckbox,
      gender,
      securityNumber
    }),
    handleSubmit(values, { props, setErrors, setSubmitting }) {
      setTimeout(() => {
        alert(JSON.stringify(values, null, 2));
        setSubmitting(false);
        localStorage.setItem("fullname", values.fullname);
        props.onChangeUser(values);
        props.history.push("/book");
      }, 100);
    },

    validate: values => {
      let errors = {};

      if (!values.phone) {
        errors.phone = "Phone is Required";
      } else {
        let pattern = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
        if (!values.phone.match(pattern)) {
          errors.phone = "Phone number is invalid";
        }
      }

      errors.securityNumber = [];
      for (let index in values.securityNumber) {
        if (
          values.securityNumber[index] === "" ||
          !numberRegex.test(values.securityNumber[index])
        )
          errors.securityNumber[index] = true;
        else errors.securityNumber[index] = false;
      }
      if (errors.securityNumber.indexOf(true) === -1) {
        errors = {};
      }

      if (!values.fullname) {
        errors.fullname = "Full Name is Required";
      }

      return errors;
    }
    // validationSchema: ({registerSchema})
  })
);
const FormRegister = ({ handleSubmit, step, nextStep, prevStep, ...props }) => (
  <form onSubmit={handleSubmit}>
    {{
      1: <Step1 nextStep={nextStep} {...props} />,
      2: <Step2 prevStep={prevStep} nextStep={nextStep} {...props} />,
      3: <Step3 {...props} />
    }[step] || <div />}
  </form>
);
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onChangeUser: auth => dispatch(actions.changeUser(auth))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(enhance(FormRegister)));
