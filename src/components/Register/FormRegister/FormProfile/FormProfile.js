import React, { Component } from "react";
import "./FormProfile.scss";
import Upload from "./../../../../assets/img/upload.png";
import rightArrow from "./../../../../assets/img/rightArrow.png";
class Step3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: "",
      imagePreviewUrl: ""
    };
    console.log(props.errors);
  }

  _handleSubmit = e => {
    e.preventDefault();
    // TODO: do something with -> this.state.file
    const { imagePreviewURL } = this.state;
    localStorage.setItem("image", imagePreviewURL);
  };

  _handleImageChange = e => {
    e.preventDefault();

    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file,
        imagePreviewUrl: reader.result
      });
    };
    localStorage.setItem("image", this.state.imagePreviewUrl);
    reader.readAsDataURL(file);
  };

  render() {
    const { imagePreviewUrl } = this.state;
    let _imagePreview = null;
    if (imagePreviewUrl) {
      _imagePreview = <img src={imagePreviewUrl} alt="" />;
    }
    const imagePreview = imagePreviewUrl ? (
      <div className="imgPreview">{_imagePreview}</div>
    ) : (
      <img src={Upload} alt="" className="profilePic" />
    );
    return (
      <div>
        <div className="coverInfo">
          <div className="Form2">
            <div className="enter1">
              <h4>Enter your Info</h4>
            </div>
            <div className="group-inline">
              <input
                type="name"
                name="fullname"
                className="coverFormInput form-control"
                placeholder="Enter your full name"
                value={this.props.values.fullname}
                onChange={this.props.handleChange}
              />
              <div>
                {this.props.errors.fullname && (
                  <div id="feedback">{this.props.errors.fullname}</div>
                )}
              </div>
            </div>

            <div className="groupGender">
              <h4 className="selectGender gray-color">Select gender</h4>
              <input
                id="male"
                type="radio"
                name="gender"
                value="male"
                onChange={this.props.handleChange}
              />
              <label className="drinkcard-cc male" htmlFor="male" />

              <input
                id="female"
                type="radio"
                name="gender"
                value="female"
                onChange={this.props.handleChange}
              />

              <label className="drinkcard-cc female" htmlFor="female" />
            </div>
            <div className="groupUpload">
              <input
                type="file"
                onChange={this._handleImageChange}
                id="upload"
                className="bg-input"
              />
              <label htmlFor="upload">
                {imagePreview}
              </label>
              <p className="profile gray-color">Upload profile picture</p>
              <button
                className="toggleButton"
                disabled={this.props.errors ? true : false}
                type="submit"
                onClick={this._handleSubmit}
              />
            </div>
            

            <div className="rightArrow1">
              <button type="submit" className="rightArrBtn">
                <img className="rightArrowImg" src={rightArrow} alt="" />
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Step3;
