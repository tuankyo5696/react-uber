import * as actionTypes from './../constants/actionTypes';

const initialState = {
    auth : localStorage  && JSON.parse(localStorage.getItem('auth'))? JSON.parse(localStorage.getItem('auth') ): ''
}

const reducer = (state = initialState ,action) => {
    switch(action.type){
        case actionTypes.CHANGE_USER:
                localStorage.setItem('auth', JSON.stringify(action.auth));
            return {
               auth : action.auth
            }
        default : return state;
    }
}

export default reducer;