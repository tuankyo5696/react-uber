import * as types from './../constants/actionTypes';


export const changeUser = (auth) =>{
    return {
        type: types.CHANGE_USER,
        auth
    }
}